var partner_stf = getParameterByName('p'),
    partner_itfx = getParameterByName('i'),
    subid_stf = getParameterByName('ps'),
    subid_itfx = getParameterByName('is'),
    landing = getParameterByName('lp');

function getParameterByName(name) {
  name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
  var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
    results = regex.exec(location.search);
  return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

if (partner_stf) {
    var params = {
      'partner':partner_stf,
      'url': window.location.origin
    },
    url = 'https://my.stforex.com/api/v1/cookie/set';
    if(subid_stf){
      params.subid = subid_stf;
    }
    var img = $('<img>', {'src' : url + '?' + jQuery.param( params )});
    $('body').append(img);
}




if (partner_itfx) {
  $('a').each(function(){
    var href = $(this).attr('href');
    if (subid_itfx){
      href = href.replace('\/partner/93608','\/partner\/'+partner_itfx+'\/'+subid_itfx);
      href = href.replace('\/partner/105257','\/partner\/'+partner_itfx+'\/'+subid_itfx);
    }
    else{
      href = href.replace('\/partner/93608','\/partner\/'+partner_itfx);
      href = href.replace('\/partner/105257','\/partner\/'+partner_itfx);
    }
    $(this).attr('href',href);
  });
}

