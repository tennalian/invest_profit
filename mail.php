<?php
function clean_string($string) {
	$bad = array("content-type","bcc:","to:","cc:","href");
	return str_replace($bad,"",$string);
}
$message = '';

foreach (array(
	'name',
	'email',
) as $key) {
	if (!empty($_POST[$key])) {
		$message .= $key . ": ".clean_string($_POST[$key]) . PHP_EOL;
	}
}

file_put_contents('sended.txt', $message . PHP_EOL, FILE_APPEND | LOCK_EX);
